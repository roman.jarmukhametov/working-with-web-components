/**
 * Retrieves the subscription status from localStorage.
 *
 * @returns {string|null} The subscription status ('true' or 'false' as a string) or null if not set.
 */
function getSubscriptionStatus() {
  return localStorage.getItem("isSubscribed");
}

/**
 * Sets the subscription status and, optionally, the subscriber's email in localStorage.
 *
 * @param {boolean} isSubscribed - The subscription status to be saved.
 * @param {string} [email] - The subscriber's email to be saved. Optional.
 */
function setSubscriptionStatus(isSubscribed, email) {
  // Convert the boolean `isSubscribed` to a string to store in localStorage.
  localStorage.setItem("isSubscribed", isSubscribed.toString());

  // If an email is provided, store it in localStorage as well.
  if (email) {
    localStorage.setItem("email", email);
  }
}

/**
 * Removes the subscription status and email from localStorage, effectively unsubscribing the user.
 */
function removeSubscriptionStatus() {
  // Remove both the subscription status and the subscriber's email from localStorage.
  localStorage.removeItem("isSubscribed");
  localStorage.removeItem("email");
}

// Export the functions to make them available for import in other modules.
export {
  getSubscriptionStatus,
  setSubscriptionStatus,
  removeSubscriptionStatus,
};
