/**
 * Represents a custom web component that serves as a flexible website section.
 * This component uses the Shadow DOM to encapsulate its style and structure,
 * providing a section that centers its slotted content both vertically and horizontally.
 *
 * @extends HTMLElement
 */
class WebsiteSection extends HTMLElement {
  /**
   * Creates an instance of WebsiteSection, attaching a shadow root with open mode.
   */
  constructor() {
    super();
    // Attach a shadow DOM tree to the instance
    this.attachShadow({ mode: "open" });
    // Call the render method to define the internal structure and styling
    this.render();
  }

  /**
   * Renders the WebsiteSection's internal HTML and CSS into its shadow root.
   * This setup uses a flexbox layout to center any slotted content.
   */
  render() {
    this.shadowRoot.innerHTML = `
          <style>
            :host {
              display: flex;
              align-items: center;
              justify-content: center;
              flex-direction: column;
            }
          </style>
          <slot></slot> <!-- Default slot for content projection -->
        `;
  }
}

// Register the custom element with the browser
customElements.define("website-section", WebsiteSection);
